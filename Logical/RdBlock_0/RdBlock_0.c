/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************/

#include <bur/plctypes.h>
#include <AsDefault.h>
#include "AsFltGen.h"

#include "APMap.h"


// Variables for Flat Stream Interface to get whole X20AP31x1 Map
_LOCAL	 X20AP31x1 	ApData;
_LOCAL	 SINT		TmpBuf[sizeof(X20AP31x1) + 16];

void _INIT TestINIT( void )
{
	BlockNum = 0;
	EnabEnergy = 1;
}

void _CYCLIC TestCYCLIC( void )
{
	// cyclically trigger generation of Energy and DFT values on AP31x1 
	if ( RBTrigDFT == TrigDFT ) 
	{
		TrigDFT ^= 1;
	}
	
	// Flat Stream Handling
	sFltRead.enable	 		= 1;
	sFltRead.cfg		 	= fltMODE_FRAME;
	sFltRead.pBuf			= (UDINT)&TmpBuf[0];
	sFltRead.bufLen			= sizeof(TmpBuf);
	sFltRead.pRxBytes	 	= (UDINT)&FltMtuRd[0];
	sFltRead.rxBytesLen 	= sizeof(FltMtuRd);
	fltRead(&sFltRead);
		if (sFltRead.status == fltERR_FRAME_FINISHED)
		{
			if (sFltRead.validBytes - 1 == sizeof(X20AP31x1))
			{
				memcpy (&ApData, &TmpBuf[1], sizeof(X20AP31x1));
			}
		}
		
	sFltWrite.enable		= 1;
	sFltWrite.cfg			= fltMODE_SYNCHRON + (1 << 4) + 2;
	sFltWrite.readSequ 		= sFltRead.readSequ;
	sFltWrite.pBuf			= (UDINT)&BlockNum;
	sFltWrite.bufLen	    = 0;
	sFltWrite.pTxBytes		= (UDINT)&FltMtuWr[0];
	sFltWrite.txBytesLen	= sizeof(FltMtuWr);
	sFltWrite.pSequ	 		= (UDINT)&FltMtuRd[0];
	fltWrite(&sFltWrite);

} // end of  _CYCLIC
